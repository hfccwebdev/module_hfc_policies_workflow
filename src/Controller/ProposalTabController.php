<?php

namespace Drupal\hfc_policies_workflow\Controller;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Datetime\DateFormatter;
use Drupal\Core\Entity\Controller\EntityViewController;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Link;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Url;
use Drupal\node\Entity\Node;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines a controller to render proposals tab for a Policy.
 *
 * @see Drupal\node\Controller\NodeViewController
 */
class ProposalTabController extends EntityViewController {

  use StringTranslationTrait;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * @var \Drupal\Core\Datetime\DateFormatter
   */
  protected $dateFormatter;

  /**
   * @var \Drupal\node\Entity\Node
   */
  protected $master;

  /**
   * @var bool
   */
  protected $active_found;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('renderer'),
      $container->get('current_user'),
      $container->get('date.formatter')
    );
  }

  /**
   * Creates a ProposalsTabController object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   The renderer service.
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   The current user.
   * @param \Drupal\Core\Datetime\DateFormatter $date_formatter
   *   The date formatter service.
   */
  public function __construct(
    EntityTypeManagerInterface $entity_type_manager,
    RendererInterface $renderer,
    AccountInterface $current_user,
    DateFormatter $date_formatter
  ) {
    parent::__construct($entity_type_manager, $renderer);
    $this->currentUser = $current_user;
    $this->dateFormatter = $date_formatter;
    $this->active_found = FALSE;
  }

  /**
   * Checks access for a specific request.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   Run access checks for this account.
   * @param \Drupal\node\Entity\Node $node
   *   Run custom access checks for this node.
   *
   * @return \Drupal\Core\Access\AccessResult
   *   The access result.
   */
  public function access(AccountInterface $account, Node $node) {
    switch ($node->getType()) {
      case 'policy':
        // @todo this should allow access if the user can view
        // policy and proposal nodes. Additional permissions checks
        // should be performed to allow link to draft proposals or to
        // create new proposals.
        return AccessResult::allowedIf($account->hasPermission('create policy_proposal content'));
    }
    return AccessResult::forbidden();
  }

  /**
   * {@inheritdoc}
   */
  public function view(EntityInterface $node, $view_mode = 'teaser') {

    $build['node'] = parent::view($node, $view_mode);

    $this->master = $node;

    $build['proposals'] = [
      ['#markup' => $this->t('<h2>Existing Proposals</h2>')],
    ];

    if ($proposals = $this->getProposals()) {
      $build['proposals'][] = [
        '#theme' => 'table',
        '#header' => [
          $this->t('ID'),
          $this->t('Policy'),
          $this->t('Policy ID'),
          $this->t('Policy Type'),
          $this->t('Status'),
          $this->t('Processed'),
        ],
        '#rows' => array_map([$this, 'getProposalRow'], $proposals),
      ];
    }
    else {
      $build['proposals'][] = ['#markup' => $this->t('<p>No matches found.</p>')];
    }

    if (!$this->active_found) {
      $build['addnew'][] = ['#markup' => $this->t('<h2>Add new proposal</h2>')];
      $url = Url::fromRoute('hfc_policies_workflow.policy_clone', ['node' => $node->id()]);
      $build['addnew'][] = Link::fromTextAndUrl('Generate new proposal', $url)->toRenderable();
    }

    return $build;
  }

  /**
   * The _title_callback for the page that renders a single node.
   *
   * @param \Drupal\Core\Entity\EntityInterface $node
   *   The current node.
   *
   * @return string
   *   The page title.
   */
  public function title(EntityInterface $node) {
    return $this->t('Proposals for @label', ['@label' => $node->label()]);
  }

  /**
   * Get related proposals.
   *
   * @return \Drupal\node\Entity\Node[]
   *   An array of related nodes.
   */
  private function getProposals() {

    // $master_type = $this->master->getType();
    $proposal_type = 'policy_proposal';
    $master_field = "field_board_policy";

    $nids = $this->entityTypeManager->getStorage('node')->getQuery()
      ->accessCheck(FALSE)
      ->condition('type', $proposal_type)
      ->condition($master_field, $this->master->id())
      ->sort('created', 'DESC')
      ->execute();

    if (!empty($nids)) {
      return Node::loadMultiple($nids);
    }
    else {
      return NULL;
    }
  }

  /**
   * Get proposal table row.
   */
  private function getProposalRow(EntityInterface $node) {

    $edit_url = Url::fromRoute('entity.node.edit_form', ['node' => $node->id()]);

    if ($node->field_proposal_processed->value == FALSE) {
      $this->active_found = TRUE;
    }

    return [
      $node->id(),
      $node->toLink(),
      $node->field_policy_id->value,
      !empty($node->field_policy_type->target_id) ? $node->field_policy_type->entity->label() : $this->t('missing'),
      $node->field_proposal_processed->value ? $this->t('Complete') : $this->t('Active'),
      $node->field_proposal_processed->value ? $this->t('Y') : Link::fromTextAndUrl('edit', $edit_url)->toString(),

    ];
  }

}
