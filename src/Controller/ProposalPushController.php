<?php

namespace Drupal\hfc_policies_workflow\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;
use Drupal\node\Entity\Node;
use Drupal\node\NodeInterface;

/**
 * Class CatalogWorkflow.
 *
 * @package Drupal\hfc_policies_workflow\Controller
 */
class ProposalPushController extends ControllerBase {

  /**
   * Checks access for a specific request.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   Run access checks for this account.
   * @param \Drupal\node\Entity\Node $node
   *   Run custom access checks for this node.
   *
   * @return \Drupal\Core\Access\AccessResult
   *   The access result.
   */
  public function access(AccountInterface $account, Node $node) {
    // Check permissions and combine that with any custom access checking needed. Pass forward
    // parameters from the route and/or request as needed.
    if ($node->getType() == 'policy_proposal') {
      return AccessResult::allowedIf(
        $account->hasPermission('push proposals') &&
        empty($node->field_proposal_processed->value)
      );
    }

    return AccessResult::forbidden();
  }

  /**
   * Push an eligible node.
   *
   * Access control check should have already verified eligibility.
   *
   * @see CatalogWorkflow::access()
   *
   * @param \Drupal\node\Entity\Node $node
   *   The source node.
   *
   * @return \Drupal\Core\Form\FormInterface
   *   Confirmation form.
   */
  public function push(NodeInterface $node) {

    if ($node->getType() == 'policy_proposal') {
      $policy = isset($node->field_board_policy->entity) ? $node->field_board_policy->entity : NULL;
      return $this->formBuilder()->getForm('Drupal\hfc_policies_workflow\Form\PolicyPushForm', $node, $policy);
    }
    else {
      return ['#type' => 'markup', '#markup' => $this->t('Unknown content type.')];
    }
  }

}
