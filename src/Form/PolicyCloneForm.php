<?php

namespace Drupal\hfc_policies_workflow\Form;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\node\NodeInterface;
use Drupal\Core\Url;
use Drupal\Core\Session\AccountInterface;
use Drupal\hfc_policies_workflow\PolicyProposalTasksInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Policy Clone confirmation form.
 */
class PolicyCloneForm extends ConfirmFormBase {

  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  private $entityTypeManager;

  /**
   * Drupal\hfc_policies_workflow\Form\PolicyProposalTasks definition.
   *
   * @var \Drupal\hfc_policies_workflow\Form\PolicyProposalTasks
   */
  private $policyProposalTasks;

  /**
   * The policy Title.
   *
   * @var string
   */
  private $title;

  /**
   * The source content type label.
   *
   * @var string
   */
  private $type;

  /**
   * The cancel URL.
   *
   * @var \Drupal\Core\Url
   */
  private $cancel_url;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('hfc_policy_proposal_tasks')
    );
  }

  /**
   * Creates a PolicyCloneForm object.
   */
  public function __construct(
    EntityTypeManagerInterface $entity_type_manager,
    PolicyProposalTasksInterface $hfc_policy_proposal_tasks
    ) {
    $this->entityTypeManager = $entity_type_manager;
    $this->policyProposalTasks = $hfc_policy_proposal_tasks;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'policy_clone_form';
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t('Are you sure you want to create a new proposal for %title?', ['%title' => $this->title]);
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return $this->cancel_url;
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return $this->t('Only do this if you are sure you want to add a new proposal!');
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return $this->t('Copy @type to Proposal', ['@type' => $this->type]);
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelText() {
    return $this->t('Cancel');
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, NodeInterface $node = NULL) {

    $this->title = $node->label();
    $this->cancel_url = $node->toUrl();
    $this->type = $node->type->entity->label();

    // Refuse to proceed if an existing proposal is found.
    if ($this->hasActiveProposals($node)) {
      $form['proposal_found'] = [
        '#markup' => $this->t(
          '<p><strong>An active proposal already exists for @label. Cannot continue.</strong></p>',
          ['@label' => $node->label()]
        ),
      ];
      $url = Url::fromRoute(
        'hfc_policies_workflow.proposal_list',
        ['node' => $node->id()]
      );
      $form['proposal_link'] = [
        '#prefix' => '<p>',
        Link::fromTextAndUrl(
          'View all proposals for this policy.',
          $url
        )->toRenderable(),
        '#suffix' => '</p>',
      ];
      return $form;
    }

    $form['policy'] = [
      '#type' => 'value',
      '#value' => $node,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $policy = $form_state->getValue('policy');

    if ($policy->getType() == 'policy') {
      // On success, returns a new unsaved proposal node.
      $node = $this->policyProposalTasks->cloneFromPolicy($policy);
    }
    else {
      $node = NULL;
    }

    if (is_object($node)) {
      $node->save();
      $form_state->setRedirect(
        'entity.node.edit_form', ['node' => $node->id()]
      );
    }
    else {
      $this->messenger()->addError(
        $this->t('Could not create requested Proposal.')
      );
    }
  }

  /**
   * Checks access for a specific request.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   Run access checks for this account.
   * @param Node $node
   *   Run custom access checks for this node.
   *
   * @return \Drupal\Core\Access\AccessResult
   *   The access result.
   */
  public function access(AccountInterface $account, NodeInterface $node) {
    switch ($node->getType()) {
      case 'policy':
        return AccessResult::allowedIf($account->hasPermission('create policy content'));
    }
    return AccessResult::forbidden();
  }

  /**
   * Check for any active proposals related to this policy.
   *
   * @param \Drupal\node\NodeInterface $policy
   *   The master node to check.
   *
   * @return bool
   *   Returns TRUE if any active proposals found.
   *
   * @todo Fix PolicyUtilities into a proper service and consolidate all of this duplicate code!!!
   */
  private function hasActiveProposals(NodeInterface $policy) {

    $proposal_type = $policy->getType() . '_proposal';

    $query = $this->entityTypeManager->getStorage('node')->getQuery()
      ->accessCheck(FALSE)
      ->condition('type', $proposal_type)
      ->condition('field_board_policy', $policy->id())
      ->condition('field_proposal_processed', FALSE);

    return !empty($query->execute());
  }

}
