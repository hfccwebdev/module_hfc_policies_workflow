<?php

namespace Drupal\hfc_policies_workflow\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerTrait;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\hfc_policies_workflow\PolicyProposalTasksInterface;
use Drupal\hfc_policies_workflow\PolicyWorkflowServiceInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class PolicyProposalCreateForm.
 *
 * @package Drupal\hfc_catalog_workflow\
 *
 * For Admin theme on this page,
 * @see https://www.drupal.org/node/2158619
 */
class PolicyProposalCreateForm extends FormBase {

  use MessengerTrait;
  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      // Load the services required to construct this class.
      $container->get('hfc_policy_proposal_tasks'),
      $container->get('hfc_policy_workflow_service')
    );
  }

  /**
   * Class constructor.
   */
  public function __construct(
    PolicyProposalTasksInterface $policy_proposal_tasks,
    PolicyWorkflowServiceInterface $policy_workflow_service
  ) {
    $this->proposalTasks = $policy_proposal_tasks;
    $this->workflowService = $policy_workflow_service;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'policy_proposal_create_form';
  }

  /**
   * Build options list of active board policies that do not already have current proposals.
   *
   * @return string[]
   *   An array of Board Policy node titles, keyed by nid.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['field_policy_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Policy ID'),
      '#description' => $this->t('This is the Policy Number.'),
      '#size' => 60,
      '#maxlength' => 255,
    ];
    $form['field_policy_name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Policy Name'),
      '#description' => $this->t('This is the Name of the Policy.'),
      '#size' => 60,
      '#maxlength' => 255,
      '#required' => TRUE,
    ];
    $form['field_policy_type'] = [
      '#type' => 'select',
      '#title' => $this->t('Policy Type'),
      '#options' => $this->workflowService->getPolicyTypeOpts(),
      '#required' => TRUE,
    ];
    $form['course_submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
      '#required' => TRUE,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $values = [
      'field_policy_id' => $form_state->getValue('field_policy_id'),
      'field_policy_name' => $form_state->getValue('field_policy_name'),
      'field_policy_type' => $form_state->getValue('field_policy_type'),
    ];
    $node = $this->proposalTasks->createNewProposal($values);

    if (is_object($node)) {
      $node->save();
      $form_state->setRedirect('entity.node.edit_form', ['node' => $node->id()]);
    }
    else {
      $this->messenger()->addError($this->t('Could not create requested Policy Proposal.'));
    }
  }

}
