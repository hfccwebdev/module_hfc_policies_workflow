<?php

namespace Drupal\hfc_policies_workflow\Form;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\hfc_policies_workflow\PolicyProposalTasksInterface;
use Drupal\node\NodeInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Policies Proposal Push confirmation form.
 */
class PolicyPushForm extends ConfirmFormBase {

  /**
   * The proposal Title.
   *
   * @var string
   */
  protected $title;

  /**
   * The cancel URL.
   *
   * @var \Drupal\Core\Url
   */
  protected $cancelUrl;

  /**
   * Drupal\Core\Session\AccountProxyInterface definition.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * Drupal\hfc_policies_workflow\PolicyProposalTasksInterface definition.
   *
   * @var \Drupal\hfc_policies_workflow\PolicyProposalTasksInterface
   */
  protected $policyProposalTasks;

  /**
   * Stores the Time service.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  private $time;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('current_user'),
      $container->get('hfc_policy_proposal_tasks'),
      $container->get('datetime.time')
    );
  }

  /**
   * Initialize the object.
   *
   * @param \Drupal\Core\Session\AccountProxyInterface $current_user
   *   The User service.
   * @param \Drupal\hfc_policies_workflow\PolicyProposalTasksInterface $policy_proposal_tasks
   *   The Policy Proposal service.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The Time service.
   */
  public function __construct(
    AccountProxyInterface $current_user,
    PolicyProposalTasksInterface $policy_proposal_tasks,
    TimeInterface $time
  ) {
    $this->currentUser = $current_user;
    $this->policyProposalTasks = $policy_proposal_tasks;
    $this->time = $time;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'policy_push_form';
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t('Do you want to push proposal %title to policy?', ['%title' => $this->title]);
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return $this->cancelUrl;
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return $this->t('Only do this if you are sure!');
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return $this->t('Push to Policy');
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelText() {
    return $this->t('Cancel');
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(
    array $form,
    FormStateInterface $form_state,
    NodeInterface $proposal = NULL,
    NodeInterface $policy = NULL
  ) {

    $this->title = $proposal->label();
    $this->cancelUrl = $proposal->toUrl();

    $form['proposal'] = [
      '#type' => 'value',
      '#value' => $proposal,
    ];

    $form['policy'] = [
      '#type' => 'value',
      '#value' => $policy,
    ];

    $form['proposal_title'] = [
      '#prefix' => '<p><strong>',
      '#markup' => $this->t(
        'Processing Policy Proposal %n: %t',
        ['%n' => $proposal->id(), '%t' => $proposal->label()]
      ),
      '#suffix' => '</strong></p>',
    ];

    if (!empty($policy)) {
      $form['policy_title'] = [
        '#prefix' => '<p>',
        '#markup' => $this->t(
          'Policy %n: %t will be updated.',
          ['%n' => $policy->id(), '%t' => $policy->label()]
        ),
        '#suffix' => '</p>',
      ];
    }
    else {
      $form['policy_new'] = [
        '#prefix' => '<p>',
        '#markup' => $this->t('A new policy will be created.'),
        '#suffix' => '</p>',
      ];
    }

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $proposal = $form_state->getValue('proposal');
    $policy = $form_state->getValue('policy');

    // On success, returns the new or existing unsaved policy node.
    $node = $this->policyProposalTasks->pushToPolicy($proposal, $policy);

    if (is_object($node)) {
      $node->save();

      if (empty($proposal->field_board_policy->target_id)) {
        $proposal->field_board_policy->target_id = $node->id();
        $proposal->field_board_policy->entity = $node;
      }

      $proposal->field_proposal_processed->setValue(TRUE);
      $proposal->setNewRevision(TRUE);
      $proposal->setRevisionCreationTime($this->time->getRequestTime());
      $proposal->setRevisionUserId($this->currentUser->id());
      $proposal->setRevisionLogMessage("Pushed Proposal to Policy.");
      $proposal->save();

      $form_state->setRedirectUrl($node->toUrl());
    }
    else {
      $this->messenger()->addError(
        $this->t('Could not create requested Proposal.')
      );
    }
  }

}
