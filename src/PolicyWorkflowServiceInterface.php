<?php

namespace Drupal\hfc_policies_workflow;

/**
 * Defines an interface for Policy Workflow.
 */
interface PolicyWorkflowServiceInterface {

  /**
   * Lists policies eligible for cloning.
   */
  public function getPolicyCloneList();

  /**
   * Find Policy Proposals.
   */
  public function findMatchingProposals($policy_id);

  /**
   * Generate base values for new content.
   *
   * @param string $type
   *   The machine_name of the desired content type.
   *
   * @return array
   *   An array of node values.
   */
  public function baseContentValues($type);

  /**
   * Retrieve list of policy type taxonomy terms.
   */
  public function getPolicyTypeOpts();

}
