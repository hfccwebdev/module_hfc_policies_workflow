<?php

namespace Drupal\hfc_policies_workflow;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Language\Language;
use Drupal\Core\Logger\LoggerChannelTrait;
use Drupal\Core\Messenger\MessengerTrait;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\node\NodeInterface;

/**
 * Service for Policy Workflow.
 */
class PolicyWorkflowService implements PolicyWorkflowServiceInterface {

  use LoggerChannelTrait;
  use MessengerTrait;
  use StringTranslationTrait;

  /**
   * Stores the taxonomy vocabulary name for policy types.
   */
  const POLICY_TYPE_VID = 'policy_section';

  /**
   * Drupal\Core\Session\AccountProxyInterface definition.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * Drupal\Core\Database\Connection definition.
   *
   * @var \Drupal\Core\Database\Connection
   */
  private $database;

  /**
   * Drupal\Core\Entity\EntityStorageInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  private $nodeStorageManager;

  /**
   * Drupal\taxonomy\TermInterface definition.
   *
   * @var \Drupal\taxonomy\TermInterface
   */
  private $termStorage;

  /**
   * Stores the Time service.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  private $time;

  /**
   * Class constructor.
   *
   * @param \Drupal\Core\Session\AccountProxyInterface $current_user
   *   The Account Proxy service.
   * @param \Drupal\Core\Database\Connection $database
   *   The Database Connection service.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The Time service.
   */
  public function __construct(
    AccountProxyInterface $current_user,
    Connection $database,
    EntityTypeManagerInterface $entity_type_manager,
    TimeInterface $time
  ) {
    $this->currentUser = $current_user;
    $this->database = $database;
    $this->nodeStorageManager = $entity_type_manager->getStorage('node');
    $this->termStorage = $entity_type_manager->getStorage('taxonomy_term');
    $this->time = $time;
  }

  /**
   * Policy Clone.
   */
  public function getPolicyCloneList() {

    $has_proposal = $this->getBoardPolicyWithActiveProposals();

    $query = $this->database->select('node', 'n');

    $query->join('node_field_data', 'd', "n.vid = d.vid");

    $query->condition('n.type', 'policy');
    $query->condition('d.status', NodeInterface::PUBLISHED);

    $query->addfield('n', 'nid');
    $query->addfield('d', 'title');
    $query->orderBy('d.title');

    if ($result = $query->execute()->fetchAll()) {
      $output = [];
      foreach ($result as $node) {
        if (!in_array($node->nid, $has_proposal)) {
          $output[$node->nid] = $node->title;
        }
      }
      return $output;
    }
  }

  /**
   * Get the Board Policy with active proposals.
   */
  private function getBoardPolicyWithActiveProposals() {
    // @todo Complete this method.
    return [];
  }

  /**
   * Find proposals that match the $policy_id.
   */
  public function findMatchingProposals($policy_id) {
    // @todo Complete this method.
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function baseContentValues($type) {
    return [
      'type' => $type,
      'changed' => $this->time->getRequestTime(),
      'status' => NodeInterface::PUBLISHED,
      'language' => Language::LANGCODE_NOT_SPECIFIED,
      'uid' => $this->currentUser->id(),
      'name' => $this->currentUser->getAccountName(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getPolicyTypeOpts() {

    $terms = $this->termStorage->loadTree(self::POLICY_TYPE_VID);
    $output = [];
    foreach ($terms as $term) {
      $output[$term->tid] = $term->name;
    }
    return $output;
  }

}
