<?php

namespace Drupal\hfc_policies_workflow;

use Drupal\node\NodeInterface;

/**
 * Defines the Policy Proposal Tasks Interface.
 */
interface PolicyProposalTasksInterface {

  /**
   * Create a Board Policy to Policy.
   *
   * @param array $values
   *   get values from Tasks.
   */
  public function createNewProposal($values);

  /**
   * Clone a Board Policy to Policy.
   *
   * @param Node $proposal
   *   An proposed policy.
   */
  public function cloneFromPolicy(NodeInterface $policy);

  /**
   * Push a Board Policy to Policy.
   *
   * @param Node $proposal
   *   An proposed policy.
   * @param Node $policy
   *   An existing Board Policy.
   */
  public function pushToPolicy(NodeInterface $proposal, NodeInterface $policy);

}
