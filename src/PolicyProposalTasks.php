<?php

namespace Drupal\hfc_policies_workflow;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Logger\LoggerChannelTrait;
use Drupal\Core\Messenger\MessengerTrait;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\node\Entity\Node;
use Drupal\node\NodeInterface;

/**
 * Defines the Policy Proposal Tasks Service.
 */
class PolicyProposalTasks implements PolicyProposalTasksInterface {

  use LoggerChannelTrait;
  use MessengerTrait;
  use StringTranslationTrait;

  /**
   * Drupal\Core\Session\AccountProxyInterface definition.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * Drupal\PolicyWorkflowService definition.
   *
   * @var \Drupal\PolicyWorkflowService
   */
  protected $hfcPolicyWorkflowService;

  /**
   * Stores the Time service.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  private $time;

  /**
   * Class constructor.
   *
   * @param \Drupal\Core\Session\AccountProxyInterface $current_user
   *   The Account Proxy service.
   * @param \Drupal\PolicyWorkflowService $hfcPolicyWorkflowService
   *   The Time service.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The Time service.
   */
  public function __construct(
    AccountProxyInterface $current_user,
    PolicyWorkflowService $hfcPolicyWorkflowService,
    TimeInterface $time
  ) {
    $this->currentUser = $current_user;
    $this->workflowService = $hfcPolicyWorkflowService;
    $this->time = $time;
  }

  /**
   * {@inheritdoc}
   */
  public function createNewProposal($values) {

    $this->messenger()->addMessage($this->t('Creating new policy proposal.'));

    $node_values = $this->workflowService->baseContentValues('policy_proposal');

    $node_values['title'] = $values['field_policy_name'];

    $node = Node::create($node_values);

    if (!empty($values['field_policy_id'])) {
      $node->field_policy_id->value = $values['field_policy_id'];
    }

    $node->field_policy_name->value = $values['field_policy_name'];
    $node->field_policy_type->target_id = $values['field_policy_type'];
    // @todo Load field settings and select default.
    $node->field_status->value = 'hold';

    $node->setNewRevision(TRUE);
    $node->setRevisionCreationTime($this->time->getRequestTime());
    $node->setRevisionUserId($this->currentUser->id());
    $node->setRevisionLogMessage('This proposal represents a new policy.');

    return $node;
  }

  /**
   * {@inheritdoc}
   */
  public function cloneFromPolicy(NodeInterface $policy) {

    $values = $this->workflowService->baseContentValues('policy_proposal');

    $proposal = Node::create($values);

    $proposal->field_board_policy->target_id = $policy->id();
    $proposal->field_board_policy->entity = $policy;

    // Basic Info.
    $proposal->title->setValue($policy->label());
    $proposal->field_adopted_date->setValue($policy->field_adopted_date->getValue());
    $proposal->body->setValue($policy->body->getValue());
    $proposal->field_policy_id->setValue($policy->field_policy_id->getValue());
    $proposal->field_policy_name->setValue($policy->field_policy_name->getValue());
    $proposal->field_policy_type->setValue($policy->field_policy_type->getValue());
    $proposal->field_rescinded_date->setValue($policy->field_rescinded_date->getValue());
    $proposal->field_rescinded_explanation->setValue($policy->field_rescinded_explanation->getValue());
    $proposal->field_revised_date->setValue($policy->field_revised_date->getValue());
    $proposal->field_status->setValue($policy->field_status->getValue());

    $proposal->setNewRevision(TRUE);
    $proposal->setRevisionCreationTime($this->time->getRequestTime());
    $proposal->setRevisionUserId($this->currentUser->id());
    $proposal->setRevisionLogMessage("This proposal was cloned from Board Policy {$policy->id()}.");

    return $proposal;
  }

  /**
   * {@inheritdoc}
   */
  public function pushToPolicy(NodeInterface $proposal, NodeInterface $policy = NULL) {

    if (empty($policy)) {
      $values = $this->workflowService->baseContentValues('policy');
      $policy = Node::create($values);
    }

    // Basic Info.
    $policy->title->setValue($proposal->title->getValue());
    $policy->field_adopted_date->setValue($proposal->field_adopted_date->getValue());
    $policy->body->setValue($proposal->body->getValue());
    $policy->field_policy_id->setValue($proposal->field_policy_id->getValue());
    $policy->field_policy_name->setValue($proposal->field_policy_name->getValue());
    $policy->field_policy_type->setValue($proposal->field_policy_type->getValue());
    $policy->field_rescinded_date->setValue($proposal->field_rescinded_date->getValue());
    $policy->field_rescinded_explanation->setValue($proposal->field_rescinded_explanation->getValue());
    $policy->field_revised_date->setValue($proposal->field_revised_date->getValue());
    $policy->field_status->setValue($proposal->field_status->getValue());

    $policy->setNewRevision(TRUE);
    $policy->setRevisionCreationTime($this->time->getRequestTime());
    $policy->setRevisionUserId($this->currentUser->id());
    $policy->setRevisionLogMessage("Syncronized with Policy Proposal {$proposal->id()}.");

    return $policy;
  }

}
